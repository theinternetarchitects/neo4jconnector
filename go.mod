module gitlab.com/theinternetarchitects/neo4jconnector

go 1.14

require (
	github.com/golang/mock v1.4.3 // indirect
	github.com/neo4j-drivers/gobolt v1.7.4 // indirect
	github.com/neo4j/neo4j-go-driver v1.7.4
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
)
