package neo4jconnector

import "github.com/neo4j/neo4j-go-driver/neo4j"

// Neo4jConnector type
type Neo4jConnector struct {
	uri     string
	err     error
	driver  neo4j.Driver
	session neo4j.Session
}

// New Create a new instance of a Neo4j Connector
func New(uri string, username string, password string) (*Neo4jConnector, error) {
	c := new(Neo4jConnector)
	c.uri = uri

	c.driver, c.err = neo4j.NewDriver(c.uri, neo4j.BasicAuth(username, password, ""))
	if c.err != nil {
		return nil, c.err
	}
	defer c.driver.Close()

	c.session, c.err = c.driver.Session(neo4j.AccessModeWrite)
	if c.err != nil {
		return nil, c.err
	}
	defer c.session.Close()

	// Example of how to perform a query
	// TODO: remove the following commented code from this function
	// greeting, err := c.StoreNode("Greeting", map[string]interface{}{"message": "test"})
	// if err != nil {
	// 	return nil, err
	// }

	return c, nil
}

// StoreNode store a node using the attached client
func (client *Neo4jConnector) StoreNode(nodeName string, data map[string]interface{}) (interface{}, error) {
	return client.session.WriteTransaction(func(transaction neo4j.Transaction) (interface{}, error) {
		result, err := transaction.Run(
			"CREATE (a:"+nodeName+") "+
				"SET a.message = $message "+
				"RETURN a.message + ', from node ' + id(a)",
			data,
		)
		if err != nil {
			return nil, err
		}

		if result.Next() {
			return result.Record().GetByIndex(0), nil
		}

		return nil, result.Err()
	})
}
